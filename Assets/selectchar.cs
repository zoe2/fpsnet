﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class selectchar : MonoBehaviour {

    public GameObject moneyMan;
    public GameObject seventySix;
    public networkingscript manager;
    public Text ipField;

    GameObject preview;

    public void moneySelect()
    {
        manager.playerClassSelect = 1;
    }
    public void lollipopSelect()
    {
        manager.playerClassSelect = 3;
    }
    public void sevSelsect()
    {
        manager.playerClassSelect = 2;
    }
    public void Host()
    {
        manager.StartHost();
    }
    public void Client()
    {
        manager.networkAddress = ipField.text;
        manager.StartClient();
    }
}
