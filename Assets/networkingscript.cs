﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class networkingscript : NetworkManager {

    public Vector3 redSpawn;
    public Vector3 blueSpawn;
    bool rb = true;
    public GameObject sev;
    public int playerClassSelect;

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
    {
        Debug.Log("we started the networking script");
        rb = !rb;
        if (rb && extraMessageReader != null)
        {
            var i = extraMessageReader.ReadMessage<StringMessage>();
           
            GameObject player = (GameObject)GameObject.Instantiate(spawnPrefabs[int.Parse(i.value)], redSpawn, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
            player.tag = "red";
            if (i.value == "1")
            {
                player.GetComponent<sevController>().RpcChangeTag("red");
            }
            else
            {
                player.GetComponent<playerController>().RpcChangeTag("red");
            }
            Debug.Log("we have blue");
        }
        else if (!rb)
        {
            var i = extraMessageReader.ReadMessage<StringMessage>();
           
            GameObject player = (GameObject)GameObject.Instantiate(spawnPrefabs[int.Parse(i.value)], blueSpawn, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
            player.tag = "blue";
            if (i.value == "1")
            {
                player.GetComponent<sevController>().RpcChangeTag("blue");
            }
            else
            {
                player.GetComponent<playerController>().RpcChangeTag("blue");
            }
            
        }
        else
        {
            Debug.Log("NO PLAYER DATA!");
        }
    }
    public override void OnClientConnect(NetworkConnection conn)
    {
        Debug.Log("Client Connect");
        if (string.IsNullOrEmpty(onlineScene) || (onlineScene == offlineScene))
        {
            ClientScene.Ready(conn);
            if (autoCreatePlayer)
            {
                ClientScene.AddPlayer(conn, 0, new StringMessage(playerClassSelect.ToString()));
            }
        }
        else {
            // player will be added when on-line scene finishes loading
        }
    }
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        Debug.Log("Client Scene Load");
        // always become ready.
        ClientScene.Ready(conn);

        if (autoCreatePlayer)
        {
            return;
        }

        bool addPlayer = false;
        if (ClientScene.localPlayers.Count == 0)
        {
            // no players exist
            addPlayer = true;
        }

        bool foundPlayer = false;
        foreach (var playerController in ClientScene.localPlayers)
        {
            if (playerController.gameObject != null)
            {
                foundPlayer = true;
                break;
            }
        }
        if (!foundPlayer)
        {
            // there are players, but their game objects have all been deleted
            addPlayer = true;
        }
        if (addPlayer)
        {
            ClientScene.AddPlayer(conn, 0, new StringMessage(playerClassSelect.ToString()));
        }
    }
}
