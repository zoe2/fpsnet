﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class sevMove : MonoBehaviour
{
    public GameObject Bprefab;
    float horizRotate = 0.0f;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.parent.gameObject.GetComponent<sevController>().isLocalPlayer)
        {

            var y = Input.GetAxis("Mouse Y");
            horizRotate += y * 150 * Time.deltaTime;
            horizRotate = Mathf.Clamp(horizRotate, -90, 45);
            Vector3 rot = transform.rotation.eulerAngles;
            rot.x = horizRotate;
            transform.rotation = Quaternion.Euler(rot);
            //  Debug.Log("our prefab location is: " + transform.rotation);
        }
    }
}
