﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour
{

    void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject;
        var health = hit.GetComponent<Health>();
        Debug.Log("got here!!!");
        Debug.Log("health: " + health);
        Debug.Log("hit: " + hit.tag);
        if (health != null && gameObject.tag != hit.tag)
        {
            health.TakeDamage(10);
            Debug.Log("tag: " + this.gameObject.tag);
        }

        Destroy(gameObject);
    }
}