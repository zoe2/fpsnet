﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class playerController : NetworkBehaviour
{
    //Variables
    public static float mouseSensitivity = 10;
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    CursorLockMode wantedMode;
    float time;



    void SetCursorState()
    {
        
        
            Cursor.lockState = CursorLockMode.Locked;
            // Hide cursor when locking
            Cursor.visible = (CursorLockMode.Locked != wantedMode);
        
        
    }
    void Start()
    {
        wantedMode = CursorLockMode.Locked;
        SetCursorState();
        if (isLocalPlayer)
        {
            transform.FindChild("Camera").gameObject.GetComponent<Camera>().enabled = (true);
        }
        if (tag == "red") {
            GetComponent<MeshRenderer>().material.color = Color.red;
        }
        else
        {
            GetComponent<MeshRenderer>().material.color = Color.blue;
        }
    }
    void Update()
    {
        if (isLocalPlayer)
        {
            time += Time.deltaTime;
            SetCursorState();

            CharacterController controller = GetComponent<CharacterController>();

            // is the controller on the ground?
            if (controller.isGrounded)
            {

                //Feed moveDirection with input.
                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);
                //Multiply it by speed.
                moveDirection *= speed;
                //Jumping
                if (Input.GetButton("Jump"))
                    moveDirection.y = jumpSpeed;

            }
            
            


            //if (Input.GetAxis("Fire2"))
            //{
            //    transform.Translate(Vector3.forward * 10000)
            //}

            var x = Input.GetAxis("Mouse X");           
            transform.Rotate(Vector3.up, x * 150 * Time.deltaTime );

            //Applying gravity to the controller
            moveDirection.y -= gravity * Time.deltaTime;
            //Making the character move
            controller.Move(moveDirection * Time.deltaTime);

            if (Input.GetAxis("Fire1") != 0 && time >= 0.5)
            {
                time = 0;
                Debug.Log("isred: " + gameObject.tag == "red");
                CmdFire(gameObject.tag == "red", bulletSpawn.position, bulletSpawn.rotation);
            }
           
        }
    }

    [Command]
    void CmdFire(bool isRed, Vector3 position, Quaternion rotation)
    {
        Debug.Log("we got here");
        Debug.Log("isred: " + isRed);

        Debug.Log("b rotation: " + rotation);
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            /*bulletSpawn.*/position,
            /*bulletSpawn.*/rotation);
        Debug.Log("b postition: " + position);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 60;
        Debug.Log("got passed adding forse");
        // Spawn the bullet on the Clients
        NetworkServer.Spawn(bullet);
        if (isRed)
        {
            RpcChangeBulletTag(bullet, "red");
        }
        else
        {
            Debug.Log("bullet is untagged or blue");
            RpcChangeBulletTag(bullet, "blue");
        }
        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }

    [ClientRpc]
    void RpcChangeBulletTag(GameObject bullet, string tag)
    {
        bullet.tag = tag;
        if(tag == "red")
        {
            bullet.GetComponent<MeshRenderer>().material.color = Color.red;
        }
        else
        {

            bullet.GetComponent<MeshRenderer>().material.color = Color.blue;
        }
    }
    
    [ClientRpc]
    public void RpcChangeTag(string tag)
    {
        Debug.Log("got rpc thing");
        this.tag = tag;
    }
}