﻿using UnityEngine;
using System.Collections;

public class lollipop : MonoBehaviour {

    void OnCollisionEnter(Collision collision)
    {
        var hit = collision.gameObject;
        var health = hit.GetComponent<Health>();
       
        if (health != null && gameObject.tag != hit.tag)
        {
            health.TakeDamage(100);
        }
    }
}
