﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections;


public class sevController : NetworkBehaviour
{
    //Variables
    public Transform gunEnd;
    public static float mouseSensitivity = 10;
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private Vector3 moveDirection = Vector3.zero;
    public ParticleSystem fire;
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    CursorLockMode wantedMode;
    float time;
    public GameObject boostSparkles;
    private LineRenderer laserLine;
    private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);
    bool truefalse;




    void SetCursorState()
    {

        Debug.Log("it works!!!");
        Cursor.lockState = CursorLockMode.Locked;
        // Hide cursor when locking
        Cursor.visible = (CursorLockMode.Locked != wantedMode);


    }
    void Start()
    {
        laserLine = GetComponent<LineRenderer>();
        wantedMode = CursorLockMode.Locked;
        SetCursorState();
        if (isLocalPlayer)
        {
            transform.FindChild("Camera").gameObject.GetComponent<Camera>().enabled = (true);
        }
        if (tag == "red")
        {
            Debug.Log("we are red");
            truefalse = true;
            GetComponent<MeshRenderer>().material.color = Color.red;
        }
        else
        {
            Debug.Log("we are blue");
            truefalse = false;
            GetComponent<MeshRenderer>().material.color = Color.blue;
        }
    }
    void Update()
    {
        if (isLocalPlayer)
        {
            time += Time.deltaTime;
            SetCursorState();

            CharacterController controller = GetComponent<CharacterController>();

            // is the controller on the ground?
            if (controller.isGrounded)
            {

                //Feed moveDirection with input.
                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);
                //Multiply it by speed.
                moveDirection *= speed;
                //Jumping
                if (Input.GetButton("Jump"))
                    moveDirection.y = jumpSpeed;

            }


            if (Input.GetAxis("Fire2") > 0)
            {
                this.transform.Translate(Vector3.up * 50 * Time.deltaTime);
                fire.Play();

            }
            fire.Stop();
            //if (Input.GetAxis("Fire2"))
            //{
            //    transform.Translate(Vector3.forward * 10000)
            //}

            var x = Input.GetAxis("Mouse X");
            transform.Rotate(Vector3.up, x * 150 * Time.deltaTime);

            //Applying gravity to the controller
            moveDirection.y -= gravity * Time.deltaTime;
            //Making the character move
            controller.Move(moveDirection * Time.deltaTime);

            if (Input.GetAxis("Fire1") != 0 && time >= 0.5)
            {
                time = 0;
                Debug.Log("isred: " + gameObject.tag == "red");
                CmdFire(truefalse, bulletSpawn.position, bulletSpawn.forward);
            }

        }
    }

    [Command]
    void CmdFire(bool isRed, Vector3 position, Vector3 forward)
    {
        if (!isRed)
        {
            Debug.Log("blue laser shot");
            StartCoroutine(ShotEffect());
            RaycastHit hit;
            Ray ray = new Ray(position, forward);
            Debug.DrawRay(position,forward, Color.green);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.tag == "red")
                {
                    Debug.Log("blue laser hit");
                    var health = hit.collider.gameObject.GetComponent<Health>();
                    health.TakeDamage(10);
                } 
            }
        }
        else
        {
            Debug.Log("red laser shot");
            StartCoroutine(ShotEffect());
            RaycastHit hit;

            Ray ray = new Ray(position, transform.forward);
            Debug.DrawRay(position, forward, Color.green);
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("red laser hit");
                if (hit.collider.gameObject.tag == "blue")
                {
                    var health = hit.collider.gameObject.GetComponent<Health>();
                    health.TakeDamage(10);
                }
            }
        }
    }

    //[ClientRpc]
    //void RpcChangeBulletTag(GameObject bullet, string tag)
    //{
    //    bullet.tag = tag;
    //    if (tag == "red")
    //    {
    //        bullet.GetComponent<MeshRenderer>().material.color = Color.red;
    //    }
    //    else
    //    {

    //        bullet.GetComponent<MeshRenderer>().material.color = Color.blue;
    //    }
    //}
    private IEnumerator ShotEffect()
    {
        Debug.Log("called");
        laserLine.enabled = true;
        Debug.Log("after called");
        yield return shotDuration;

        laserLine.enabled = false;
    }

    [ClientRpc]
    public void RpcChangeTag(string tag)
    {
        Debug.Log("got rpc thing");
        this.tag = tag;
    }
}