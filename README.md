# FPSNet
#### This game is a project I did last summer. I created it to learn about unity.

## What is it?
#### FPSNet is a multiplayer team based FPS game built with unity and written in C#. It has different characters (but only one of them works)

## How to star it
####Click play or build then type in the host ip or leave it blank to be the host. then select the player you want to be. (lollipop is the only working player at this moment)

## Objective
#### kill everyone on the other team.

## To Do:
#### Add more players
#### fix lollipop only bug
